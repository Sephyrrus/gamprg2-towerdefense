// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "Projectile_AreaOfEffect.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AProjectile_AreaOfEffect : public AProjectile
{
	GENERATED_BODY()
	
protected:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void CreateBlast(class AEnemy* Target);
};
