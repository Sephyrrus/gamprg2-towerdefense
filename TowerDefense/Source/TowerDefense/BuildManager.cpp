// Fill out your copyright notice in the Description page of Project Settings.


#include "BuildManager.h"
#include "TDPlayer.h"
#include "TowerBuildNode.h"
#include "TowerDataAsset.h"
#include "Tower.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Materials/Material.h"

// Sets default values
ABuildManager::ABuildManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABuildManager::BeginPlay()	
{
	Super::BeginPlay();
	Player = Cast<ATDPlayer>(GetWorld()->GetFirstPlayerController()->GetPawn());
	for (int x = 0; x < BuildNodes.Num(); x++) {
		BuildNodes[x]->OnClicked.AddDynamic(this, &ABuildManager::OnNodeClicked);
	}
}

void ABuildManager::OnNodeClicked(AActor* Actor, FKey Key) {
	SelectedBuildNode = Cast<ATowerBuildNode>(Actor);
	SelectedBuildNode->StaticMesh->SetMaterial(0, CanPurchase); // For Visual purposes (using Canpurchase as green for active)
	SelectedBuildNode->isClicked = true;
	if (!SelectedBuildNode->BuiltTower) {
		OpenShop(SelectedBuildNode);
	}
	else  {
		OpenUpgradeSell(SelectedBuildNode);
	}
}

// Called every frame
void ABuildManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

class ATower* ABuildManager::SpawnTower(TSubclassOf<class ATower> TowerType) {
	FActorSpawnParameters SpawnParams;
	ATower* SpawnedTower = GetWorld()->SpawnActor<ATower>(TowerType, SelectedBuildNode->GetActorLocation(), SelectedBuildNode->GetActorRotation(), SpawnParams);
	SpawnedTower->isActive = true;
	SelectedBuildNode->BuiltTower = SpawnedTower;
	return SpawnedTower;
}

void ABuildManager::BuyTower(int TowerIndex) {
	if (!SelectedBuildNode) return;
	if (SelectedBuildNode->BuiltTower) return;

	if (Player->GetGold() >= Towers[TowerIndex]->TowerCost) {
		Player->LoseGold(Towers[TowerIndex]->TowerCost);
		ATower *SpawnedTower = SpawnTower(Towers[TowerIndex]->TowerType);
		SpawnedTower->TowerData = Towers[TowerIndex];

		if (!TowerGhost) return;
		DestroyTowerGhost();
	}
	CloseUI();
}	

void ABuildManager::UpgradeTower() {
	if (Player->GetGold() >= SelectedBuildNode->BuiltTower->UpgradeCost) {
		Player->LoseGold(SelectedBuildNode->BuiltTower->UpgradeCost);
		SelectedBuildNode->BuiltTower->UpgradeTower();
		CloseUI();
	}
}

void ABuildManager::SellTower() {
	Player->AddGold(SelectedBuildNode->BuiltTower->GetSellPrice());
	SelectedBuildNode->BuiltTower->Destroy();
	SelectedBuildNode->BuiltTower = NULL;
	CloseUI();
}

void ABuildManager::Deselect() {
	SelectedBuildNode->StaticMesh->SetMaterial(0, DefaultMat);
	SelectedBuildNode->isClicked = false;
	SelectedBuildNode = NULL;
}

void ABuildManager::SpawnTowerGhost(int TowerIndex) {
	if (!SelectedBuildNode) return;
	FActorSpawnParameters SpawnParams;

	TowerGhost = GetWorld()->SpawnActor<ATower>(Towers[TowerIndex]->TowerType, SelectedBuildNode->GetActorLocation(), SelectedBuildNode->GetActorRotation(), SpawnParams);
	if (!TowerGhost) return;
	TowerGhost->ChangeMaterial(CanPurchase);
	TowerGhost->isActive = false;
	if (Player->GetGold() < Towers[TowerIndex]->TowerCost) {
		TowerGhost->ChangeMaterial(CantPurchase);
	}
}

void ABuildManager::DestroyTowerGhost() {
	if (TowerGhost) {
		TowerGhost->Destroy();
	}
}

