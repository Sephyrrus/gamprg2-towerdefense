// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Buff.generated.h"

UCLASS()
class TOWERDEFENSE_API ABuff : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuff();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BaseDuration;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool hasDuration;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float CurrentDuration;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		AActor* BuffHolder;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* BuffSource;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float BuffStrength;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool hasAffected;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void BuffEffect();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void BuffLost();

	UFUNCTION(BlueprintCallable)
		void RefreshBuffDuration();

	void TriggerBuff();
	
};
