// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCore.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Enemy.h"
#include "Engine/World.h"
#include "TDPlayer.h"
#include "Engine.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABaseCore::ABaseCore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));

	EntryTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Entry Trigger"));
	EntryTrigger->OnComponentBeginOverlap.AddDynamic(this, &ABaseCore::OnOverlapBegin);
}

// Called when the game starts or when spawned
void ABaseCore::BeginPlay()
{
	Super::BeginPlay();
	Player = Cast<ATDPlayer>(GetWorld()->GetFirstPlayerController()->GetPawn());
}

// Called every frame
void ABaseCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseCore::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (AEnemy* Enemy = Cast<AEnemy>(OtherActor)) {
		// Damage Health
		Player->OnEnemyEntered(Enemy->GetUnitDamage());
		OnBaseCoreEntered.Broadcast(this);
		Enemy->EnterBaseCore();
	}
}
