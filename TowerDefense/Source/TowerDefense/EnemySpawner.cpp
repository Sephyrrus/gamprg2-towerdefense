// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemySpawner.h"
#include "Enemy.h"
#include "Waypoint.h"
#include "Engine/World.h"
#include "Engine/Engine.h"

// Sets default values
AEnemySpawner::AEnemySpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemySpawner::BeginPlay()
{
	Super::BeginPlay();
}


class AEnemy* AEnemySpawner::SpawnUnit(TSubclassOf<class AEnemy> EnemyType)
{
	FActorSpawnParameters SpawnParameters;
	
	AEnemy* SpawnedEnemy = GetWorld()->SpawnActor<AEnemy>(EnemyType, GetActorLocation(), GetActorRotation(), SpawnParameters);
	if (SpawnedEnemy) {
		SpawnedEnemy->PathPoints = SpawnerPathPoints;
		SpawnedEnemy->MoveToWaypoint();
	}	
	return SpawnedEnemy;
}