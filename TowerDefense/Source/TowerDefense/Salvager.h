// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "Salvager.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ASalvager : public ATower
{
	GENERATED_BODY()
	
protected: 
	virtual void BeginPlay() override;
	virtual void AcquireTarget(AActor* Target) override;
	virtual void LoseTarget(AActor* Target) override;
	virtual void TowerAction() override ;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<class AEnemy*> EnemiesInRange;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class ATDPlayer* Player;

	void GenerateConstantGold();

public:
	UFUNCTION()
		void GenerateGold();

	UFUNCTION(BlueprintCallable)
		void OnBuild();

	UFUNCTION(BlueprintCallable)
		void OnTowerDestroy();
};
