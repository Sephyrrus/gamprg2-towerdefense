// Fill out your copyright notice in the Description page of Project Settings.


#include "TDPlayer.h"
#include "Health.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "TowerDataAsset.h"

// Sets default values
ATDPlayer::ATDPlayer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Health = CreateDefaultSubobject<UHealth>(TEXT("Health"));
	OnSalvagerBuilt.AddDynamic(this, &ATDPlayer::GainSalvagerBonus);
	OnSalvagerDestroyed.AddDynamic(this, &ATDPlayer::LoseSalvagerBonus);
}

// Called when the game starts or when spawned
void ATDPlayer::BeginPlay()
{
	Super::BeginPlay();
	Health->SetMaxHealth(Health->maxHealth);
	PlayerIncome();
}

// Called every frame
void ATDPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

int ATDPlayer::GetGold()
{
	return Gold;
}

void ATDPlayer::AddGold(int value) {
	Gold += value;
}

void ATDPlayer::LoseGold(int value) {
	Gold -= value;
}

void ATDPlayer::OnEnemyEntered(float damage)
{
	Health->TakeDamage(damage);
}

void ATDPlayer::PlayerIncome() {
	FTimerHandle TimerHandler;
	FTimerDelegate TimerDel;

	AddGold(ConstantGoldIncome);

	TimerDel.BindLambda([this] {
		PlayerIncome();
		});
	GetWorldTimerManager().SetTimer(TimerHandler, TimerDel, 3.0f, false);
}

void ATDPlayer::GainSalvagerBonus(float value) {
	ConstantGoldIncome += value;
}

void ATDPlayer::LoseSalvagerBonus(float value) {
	ConstantGoldIncome -= value;
}