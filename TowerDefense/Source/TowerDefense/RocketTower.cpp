// Fill out your copyright notice in the Description page of Project Settings.


#include "RocketTower.h"
#include "Enemy.h"

void ARocketTower::AcquireTarget(AActor* Target) {

	AEnemy* Enemy = Cast<AEnemy>(Target);

	if (!Enemy) return;

	if (Enemy->enemyType != Flying) {
		hasTarget = true;
		EnemiesInRange.Add(Enemy);
	}
}