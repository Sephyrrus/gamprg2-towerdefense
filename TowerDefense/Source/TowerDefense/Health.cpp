// Fill out your copyright notice in the Description page of Project Settings.


#include "Health.h"

// Sets default values for this component's properties
UHealth::UHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealth::BeginPlay()
{
	Super::BeginPlay();
}

void UHealth::SetMaxHealth(float value) {
	CurrentHealth = value;
}

void UHealth::TakeDamage(float value) {
	CurrentHealth -= value;
	if (CurrentHealth <= 0) {
		Die();
		CurrentHealth = 0; // i dont like negative values lang on the ui sir
	}
}

void UHealth::GainHealth(float value) {
	CurrentHealth += value;

	if (CurrentHealth > maxHealth) {
		CurrentHealth = maxHealth;
	}
}

void UHealth::Die() {
	if (isAlive == true) {
		isAlive = false;
		OnDeath.Broadcast();
	}
}

float UHealth::GetCurrentHealth() {
	return CurrentHealth;
}

