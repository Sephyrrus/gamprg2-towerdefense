// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CombatTower.h"
#include "LaserTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ALaserTower : public ACombatTower
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ABuff> ProjectileBuffReference;

	virtual void SortTargets();
	bool CheckBurn(class AEnemy* Target);
};
