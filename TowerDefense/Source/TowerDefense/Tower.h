// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Tower.generated.h"
UCLASS()
class TOWERDEFENSE_API ATower : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATower();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void AcquireTarget(AActor* Target);
	virtual void LoseTarget(AActor* Target);
	virtual void TowerAction();
	void InitializeTower();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UArrowComponent* FirePoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* TowerBaseMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* TowerGunMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* SceneComponent;

	bool hasTarget = false;
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapExit(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	UFUNCTION()
		void LookAtTarget(FVector TargetLocation);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* UpgradedMat;

	void GetOverlapping();
public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UTowerDataAsset* TowerData;

	UFUNCTION(BlueprintCallable)
		virtual void UpgradeTower();

	void ChangeMaterial(class UMaterial* Material);

	UFUNCTION(BlueprintCallable)
	int GetSellPrice();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool isActive = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tower Stats")
		FString TowerName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tower Stats")
		int UpgradeCost = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tower Stats")
		int TowerCost = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tower Stats")
		float TowerPower = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tower Stats")
		float TowerFireRate = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tower Stats")
		int MaxUpgrades;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Tower Stats")
		int CurrentUpgrades = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Tower Stats")
		class USphereComponent* TowerRange;
};
