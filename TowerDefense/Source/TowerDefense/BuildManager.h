// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BuildManager.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUnableToPurchase, ABuildManager*, BuildManager);
UCLASS()
class TOWERDEFENSE_API ABuildManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABuildManager();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATDPlayer* Player;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* CanPurchase;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* CantPurchase;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UMaterial* DefaultMat;

	UFUNCTION(BlueprintCallable)
		void OnNodeClicked(AActor* Actor, FKey Key);

	UFUNCTION(BlueprintImplementableEvent)
		void OpenShop(ATowerBuildNode* BuildNode);

	UFUNCTION(BlueprintImplementableEvent)
		void OpenUpgradeSell(ATowerBuildNode* BuildNode);
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATowerBuildNode* SelectedBuildNode;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class ATowerBuildNode*> BuildNodes;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATower* TowerGhost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class UTowerDataAsset*> Towers;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void CloseUI();

	UFUNCTION(BlueprintCallable)
		void Deselect();

	UFUNCTION(BlueprintCallable)
		void BuyTower(int TowerIndex);

	UFUNCTION(BlueprintCallable)
		void UpgradeTower();

	UFUNCTION(BlueprintCallable)
		void SellTower();
	
	UFUNCTION(BlueprintCallable)
		class ATower* SpawnTower(TSubclassOf<class ATower> TowerType);

	UFUNCTION(BlueprintCallable)
		void SpawnTowerGhost(int TowerIndex);

	UFUNCTION(BlueprintCallable)
		void DestroyTowerGhost();

	UPROPERTY(BlueprintAssignable, Category = "Event dispatchers")
		FOnUnableToPurchase OnUnableToPurchase;


};
