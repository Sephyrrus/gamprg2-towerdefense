// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerBuildNode.generated.h"

UCLASS()
class TOWERDEFENSE_API ATowerBuildNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerBuildNode();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* SceneComponent;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ABuildManager* BuildManager;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATower* BuiltTower;

	// for material change
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isClicked = false;

};
