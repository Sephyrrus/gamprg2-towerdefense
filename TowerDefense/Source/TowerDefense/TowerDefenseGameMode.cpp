// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerDefenseGameMode.h"

#include "EnemySpawner.h"
#include "Enemy.h"
#include "BuildManager.h"
#include "TDPlayer.h"
#include "WaveData.h"
#include "Health.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "TimerManager.h"

ATowerDefenseGameMode::ATowerDefenseGameMode()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ATowerDefenseGameMode::BeginPlay()
{
	Super::BeginPlay();
	Player = Cast<ATDPlayer>(GetWorld()->GetFirstPlayerController()->GetPawn());

	Player->Health->OnDeath.AddDynamic(this, &ATowerDefenseGameMode::GameOver);
}

void ATowerDefenseGameMode::WaveFinish(int currentWave) {
	Player->AddGold(Waves[currentWave]->WaveClearReward);
	CurrentWaveUnitCount = 0;
	CurrentUnitKillCount = 0;
	if (hasFinalWaveSpawned) {
		Victory();
		return;
	}
	if (currentWave <= Waves.Num()) {
		CurrentWaveCount++;
		StartWave();
	}
}

void ATowerDefenseGameMode::SpawnEnemy(AEnemySpawner* EnemySpawner, TSubclassOf<class AEnemy> EnemyRef, int EnemyCount) {
	FTimerHandle TimerHandler;
	FTimerDelegate TimerDel;
	if (EnemyCount != 0) {
		EnemyCount--;
		AEnemy* Enemy = EnemySpawner->SpawnUnit(EnemyRef);

		Enemy->Health->SetMaxHealth(Enemy->Health->maxHealth + ((Enemy->Health->maxHealth * 0.1) * CurrentWaveCount ));

		Enemy->Health->OnDeath.AddDynamic(this, &ATowerDefenseGameMode::OnEnemyKilled);
		Enemy->OnBaseEntered.AddDynamic(this, &ATowerDefenseGameMode::OnEnemyDestroyed);

		TimerDel.BindLambda([this, EnemySpawner, EnemyRef, EnemyCount] {
			SpawnEnemy(EnemySpawner, EnemyRef, EnemyCount);
			});
		GetWorldTimerManager().SetTimer(TimerHandler, TimerDel, 3.0f, false);
	}
	
}

void ATowerDefenseGameMode::StartWave() {
	if (CurrentWaveCount == Waves.Num() - 1) {
		hasFinalWaveSpawned = true;
	}
	for (int x = 0; x < Waves[CurrentWaveCount]->Enemies.Num(); x++) {
		SpawnEnemy(
			Cast<AEnemySpawner>(Spawners[Waves[CurrentWaveCount]->Enemies[x].EnemySpawnPoint]),
			Waves[CurrentWaveCount]->Enemies[x].Enemy,
			Waves[CurrentWaveCount]->Enemies[x].EnemyCount
		);
		CurrentWaveUnitCount += Waves[CurrentWaveCount]->Enemies[x].EnemyCount;
	}
}

void ATowerDefenseGameMode::OnEnemyDestroyed(AEnemy* Enemy) {
	UpdateKillCount();
}

void ATowerDefenseGameMode::OnEnemyKilled() {
	UpdateKillCount();
	Player->AddGold(Waves[CurrentWaveCount]->KillReward);
}

void ATowerDefenseGameMode::UpdateKillCount() {
	CurrentUnitKillCount++;
	if (CurrentUnitKillCount == CurrentWaveUnitCount) {
		WaveFinish(CurrentWaveCount);
	}
}
