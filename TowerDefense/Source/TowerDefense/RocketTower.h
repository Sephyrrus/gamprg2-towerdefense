// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CombatTower.h"
#include "RocketTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ARocketTower : public ACombatTower
{
	GENERATED_BODY()
protected:
	virtual void AcquireTarget(AActor* Target) override;
};
