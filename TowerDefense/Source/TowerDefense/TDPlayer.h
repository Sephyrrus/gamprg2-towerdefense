// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDPlayer.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSalvagerBuilt, float , value);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSalvagerDestroyed, float , value);

UCLASS()
class TOWERDEFENSE_API ATDPlayer : public APawn
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDPlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int Gold = 500;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isPlacing;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		class UHealth* Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float ConstantGoldIncome = 0;

	UFUNCTION(BlueprintCallable)
		int GetGold();

	UFUNCTION(BlueprintImplementableEvent)
		void TowerPlacement(TSubclassOf<class ATower> TowerType);
	UFUNCTION()
		void LoseSalvagerBonus(float value);
	UFUNCTION()
		void GainSalvagerBonus(float value);


	void AddGold(int value);
	void LoseGold(int value);
	void PlayerIncome();
	void OnEnemyEntered(float damage);
	

	UPROPERTY(BlueprintAssignable, Category = "Event dispatchers")
		FOnSalvagerBuilt OnSalvagerBuilt;

	UPROPERTY(BlueprintAssignable, Category = "Event dispatchers")
		FOnSalvagerDestroyed OnSalvagerDestroyed;

};
