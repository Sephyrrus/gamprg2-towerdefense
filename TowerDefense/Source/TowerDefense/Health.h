// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Health.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENSE_API UHealth : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealth();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	float CurrentHealth;
	
	void Die();
	bool isAlive = true;

public:	
	UPROPERTY(EditAnywhere)
		float maxHealth; // Base Health
	void SetMaxHealth(float value);
	void GainHealth(float value);

	UFUNCTION(BlueprintCallable)
	void TakeDamage(float value);

	UFUNCTION(BlueprintCallable)
	float GetCurrentHealth();

	UPROPERTY(BlueprintAssignable, Category = "Event dispatchers")
		FOnDeath OnDeath;
};
