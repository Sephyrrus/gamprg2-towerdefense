// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "BuffTower.generated.h"


/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ABuffTower : public ATower
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<class AActor*> TargetsInRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ABuff> BuffRef;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isSupport;

	void ApplyBuff();

	UFUNCTION(BlueprintCallable)
		void RemoveBuff(AActor* Target);

	bool hasBuff(class AActor* Target);

	virtual void AcquireTarget(AActor* Target) override;
	virtual void LoseTarget(AActor* Target) override;
	virtual void TowerAction() override;
	virtual void UpgradeTower() override;

public: 
	virtual void Tick(float DeltaTime) override;
};
