// Fill out your copyright notice in the Description page of Project Settings.


#include "LaserTower.h"
#include "Buff.h"
#include "Health.h"
#include "Enemy.h"
void ALaserTower::SortTargets() {

	if (!PriorityTarget || PriorityTarget->Health->GetCurrentHealth() <= 0) {
		PriorityTarget = EnemiesInRange[0];
	}

	if (CheckBurn(PriorityTarget)) { // if burning, change target
		for (int x = 0; x < EnemiesInRange.Num(); x++) {
			if (!CheckBurn(EnemiesInRange[x])) {
				PriorityTarget = EnemiesInRange[x];
			}
		}
	}
}

bool ALaserTower::CheckBurn(class AEnemy* Target) {
	TArray<AActor*> tempChildActors;

	Target->GetAttachedActors(tempChildActors, true);

	for (int x = 0; x < tempChildActors.Num(); x++) {
		if (tempChildActors[x]->IsA(ProjectileBuffReference)) {
			return true;
		}
		return false;
	}
	return false;
}