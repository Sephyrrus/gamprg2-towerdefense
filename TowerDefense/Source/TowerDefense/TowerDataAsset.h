// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TowerDataAsset.generated.h"


/**
 * 
 */
UCLASS(BlueprintType)
class TOWERDEFENSE_API UTowerDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString TowerName;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class ATower> TowerType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<int> TowerRange;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<float> TowerPower;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<float> TowerFireRate;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int TowerCost;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<int> UpgradeCost;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int MaxUpgrades;
};
