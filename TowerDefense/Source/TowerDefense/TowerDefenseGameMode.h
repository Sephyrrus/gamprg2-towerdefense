// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TowerDefenseGameMode.generated.h"

UCLASS()
class TOWERDEFENSE_API ATowerDefenseGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATowerDefenseGameMode();

protected: 
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stat Holder")
		class ATDPlayer* Player;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Waves")
		TArray<class UWaveData*> Waves;

	UFUNCTION()
		void WaveFinish(int currentWave);

	void UpdateKillCount();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Current Wave Details")
	bool hasFinalWaveSpawned = false;
public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Current Wave Details")
		int CurrentWaveCount = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Current Wave Details")
		float CurrentWaveUnitCount;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Current Wave Details")
		float CurrentUnitKillCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spawners")
		TArray<class AActor*> Spawners;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ABuildManager* BuildManager;

	UFUNCTION(BlueprintCallable)
		void SpawnEnemy(AEnemySpawner* EnemySpawner, TSubclassOf<class AEnemy> EnemyRef, int EnemyCount);

	UFUNCTION(BlueprintCallable)
		void StartWave();

	UFUNCTION()
		void OnEnemyDestroyed(AEnemy* Enemy);

	UFUNCTION()
		void OnEnemyKilled();

	UFUNCTION(BlueprintImplementableEvent)
		void GameOver();

	UFUNCTION(BlueprintImplementableEvent)
		void Victory();
};
