// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffTower.h"
#include "Buff.h"
#include "Enemy.h"


void ABuffTower::BeginPlay() {
	Super::BeginPlay();
}


void ABuffTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABuffTower::AcquireTarget(AActor* Target) {
	if (Target != this) {
		if (isSupport) {
			if (Target->IsA(ATower::StaticClass())) {
				TargetsInRange.Add(Target);
				TowerAction();
			}
		}
		else if (!isSupport) {
			if (Target->IsA(AEnemy::StaticClass())) {
				TargetsInRange.Add(Target);
				TowerAction();
			}
		}
	}
}

void ABuffTower::LoseTarget(AActor* Target) {
	RemoveBuff(Target);
	TargetsInRange.Remove(Target);
}

void ABuffTower::TowerAction() {
	if (isActive) {
		ApplyBuff();
	}
}

void ABuffTower::ApplyBuff() {
	FAttachmentTransformRules rules(EAttachmentRule::KeepRelative, false);
	FActorSpawnParameters SpawnParams;
	for (int x = 0; x < TargetsInRange.Num(); x++) {
		if (hasBuff(TargetsInRange[x]) == false) {
			ABuff* SpawnedBuff = GetWorld()->SpawnActor<ABuff>(BuffRef, TargetsInRange[x]->GetActorLocation(), TargetsInRange[x]->GetActorRotation(), SpawnParams);
			SpawnedBuff->BuffStrength = TowerPower; // Will Rename Tower Damage to Tower Power
			SpawnedBuff->AttachToActor(TargetsInRange[x], rules);
			SpawnedBuff->BuffHolder = TargetsInRange[x];
			SpawnedBuff->BuffSource = this; // Debug
		
			if (SpawnedBuff->hasDuration) {
				SpawnedBuff->BaseDuration += (SpawnedBuff->BaseDuration / 4) * CurrentUpgrades;
			}

			SpawnedBuff->TriggerBuff();
		}
	}
	
}

void ABuffTower::RemoveBuff(AActor* Target) {
	if (hasBuff(Target) == true) {
		TArray<AActor*> tempChildActors;
		Target->GetAttachedActors(tempChildActors, true);

		for (int x = 0; x < tempChildActors.Num(); x++) {

			ABuff* Buff = Cast<ABuff>(tempChildActors[x]);
			if (Buff) {
				Buff->BuffLost();
			}
		}
	}
}

bool ABuffTower::hasBuff(class AActor* Target) {
	TArray<AActor*> tempChildActors;

	Target->GetAttachedActors(tempChildActors,true);

	for (int x = 0; x < tempChildActors.Num(); x++) {
		if (tempChildActors[x]->IsA(BuffRef)) {
			return true;
		}
		return false;
	}
	return false;
}

void ABuffTower::UpgradeTower() {
	Super::UpgradeTower();

	// To Refresh the newly updated buff

	for (int x = 0; x < TargetsInRange.Num(); x++) {
		RemoveBuff(TargetsInRange[x]);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Buff Reset"));
	}
	ApplyBuff();
}