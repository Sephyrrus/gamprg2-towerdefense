// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile_Buff.h"
#include "Buff.h"
#include "Enemy.h"
#include "Tower.h"

void AProjectile_Buff::ApplyBuff(AActor* Target) {
	FAttachmentTransformRules rules(EAttachmentRule::KeepRelative, false);
	FActorSpawnParameters SpawnParams;

	if (hasBuff(Target) == false) {
		ABuff* SpawnedBuff = GetWorld()->SpawnActor<ABuff>(BuffToSpawn, FVector(0), FRotator(0), SpawnParams);
		SpawnedBuff->AttachToActor(Target, rules);
		SpawnedBuff->BuffStrength = Damage;
		SpawnedBuff->BuffHolder = Target;
	}
	else {
		TArray<AActor*> tempChildActors;

		Target->GetAttachedActors(tempChildActors, true);

		for (int x = 0; x < tempChildActors.Num(); x++) {

			if (tempChildActors[x]->IsA(BuffToSpawn)) {
				ABuff* Buff = Cast<ABuff>(tempChildActors[x]);
				Buff->RefreshBuffDuration();
			}
		}
	}

	Destroy();
}


bool AProjectile_Buff::hasBuff(AActor* Target) {
	TArray<AActor*> tempChildActors;

	Target->GetAttachedActors(tempChildActors, true);

	for (int x = 0; x < tempChildActors.Num(); x++) {
		if (tempChildActors[x]->IsA(BuffToSpawn)) {
			return true;
		}
		return false;
	}
	return false;
}
