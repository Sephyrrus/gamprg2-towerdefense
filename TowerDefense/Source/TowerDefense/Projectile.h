// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Projectile.generated.h"
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEnemyHit);
UCLASS()
class TOWERDEFENSE_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AProjectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UProjectileMovementComponent* ProjectileMovement;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	void SetBulletDuration();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void InitializeProjectile(float Damage);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATower* Shooter; // Owner

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float Damage;

	UFUNCTION(BlueprintImplementableEvent)
		void ProjectileEffect(class AEnemy* Target);

	UPROPERTY(BlueprintAssignable, Category = "Event dispatchers")
		FOnEnemyHit OnEnemyHit;
};
