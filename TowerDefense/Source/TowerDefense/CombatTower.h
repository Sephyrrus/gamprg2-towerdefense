// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tower.h"
#include "CombatTower.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ACombatTower : public ATower
{
	GENERATED_BODY()
	
protected: 
	virtual void BeginPlay() override;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<class AEnemy*> EnemiesInRange;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class AEnemy* PriorityTarget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AProjectile> ProjectileRef;

	bool CanShoot = true;

	virtual void AcquireTarget(AActor* Target) override;
	virtual void LoseTarget(AActor* Target) override;
	virtual void TowerAction() override;
	virtual void SortTargets();

	void Fire();
public:
	virtual void Tick(float DeltaTime) override;
};
