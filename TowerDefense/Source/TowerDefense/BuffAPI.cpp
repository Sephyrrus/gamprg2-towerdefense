// Fill out your copyright notice in the Description page of Project Settings.


#include "BuffAPI.h"
#include "Enemy.h"
#include "Tower.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "TimerManager.h"

// Sets default values for this component's properties
UBuffAPI::UBuffAPI()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBuffAPI::BeginPlay()
{
	Super::BeginPlay();

	TriggerBuff();
}


void UBuffAPI::TriggerBuff() {

	BuffEffect();

	if (hasDuration) {
		FTimerHandle TimerHandler;
		FTimerDelegate TimerDel;

		TimerDel.BindLambda([this] {
			this->RemoveFromRoot();
			});

		//GetWorldTimerManager().SetTimer(TimerHandler, TimerDel, duration, false);
	}

}


// Called every frame
void UBuffAPI::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

