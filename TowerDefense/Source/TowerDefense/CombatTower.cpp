// Fill out your copyright notice in the Description page of Project Settings.


#include "CombatTower.h"
#include "Projectile.h"
#include "Components/ArrowComponent.h"
#include "Enemy.h"
#include "TowerDataAsset.h"
#include "Health.h"

void ACombatTower::BeginPlay() {
	Super::BeginPlay();
}

void ACombatTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (hasTarget && EnemiesInRange.Num() != 0 && isActive) {
		SortTargets();
		LookAtTarget(PriorityTarget->GetActorLocation());
		TowerAction();
	}
}

void ACombatTower::SortTargets() {

	if (!PriorityTarget || PriorityTarget->Health->GetCurrentHealth() <= 0) {
		PriorityTarget = EnemiesInRange[0];
	}

	for (int x = 0; x < EnemiesInRange.Num(); x++) {
		
		float PriorityTargetPosition = FVector::Distance(PriorityTarget->GetActorLocation(), GetActorLocation());
		
		float NextTargetPosition = FVector::Distance(EnemiesInRange[x]->GetActorLocation(), GetActorLocation());
		
		if (PriorityTargetPosition > NextTargetPosition) {
			EnemiesInRange[0] = EnemiesInRange[x];
		}
	}
}

void ACombatTower::AcquireTarget(AActor* Target) {
	AEnemy* Enemy = Cast<AEnemy>(Target);
	if (Enemy) {
		hasTarget = true;
		EnemiesInRange.Add(Enemy);
	}
}

void ACombatTower::LoseTarget(AActor* Target) {
	if (!hasTarget) return;
	AEnemy* Enemy = Cast<AEnemy>(Target);

	if (Enemy) {
		EnemiesInRange.Remove(Enemy);
		if (EnemiesInRange.Num() != 0) {
			PriorityTarget = EnemiesInRange[0];
		}
	}
	
	if (EnemiesInRange.Num() == 0) hasTarget = false;
}

void ACombatTower::TowerAction() {
	if (!CanShoot) return;
	Fire();
}

void ACombatTower::Fire() {
	FTimerHandle TimerHandler;
	FTimerDelegate TimerDel;
	FActorSpawnParameters SpawnParams;
	CanShoot = false;

	AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileRef,
														FirePoint->GetComponentLocation(),
														FirePoint->GetComponentRotation(),
														SpawnParams);
	Projectile->InitializeProjectile(TowerPower);
	Projectile->Shooter = this;
	Projectile->SetActorRotation(FRotator(TowerGunMesh->GetRelativeRotation().Roll, TowerGunMesh->GetRelativeRotation().Pitch + 180, TowerGunMesh->GetRelativeRotation().Yaw));

	TimerDel.BindLambda([this] {
		CanShoot = true;
		});
	GetWorldTimerManager().SetTimer(TimerHandler, TimerDel, TowerFireRate, false);

	


}