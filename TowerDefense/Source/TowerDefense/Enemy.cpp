// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "Health.h"
#include "Components/StaticMeshComponent.h"
#include "Waypoint.h"
#include "EnemyAIController.h"
#include "GameFrameWork/FloatingPawnMovement.h"
#include "Components/BoxComponent.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));

	Movement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("Movement"));

	Health = CreateDefaultSubobject<UHealth>(TEXT("Health"));

	HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox"));
	HitBox->SetupAttachment(StaticMesh);

	Health->OnDeath.AddDynamic(this, &AEnemy::Death);
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	Movement->MaxSpeed = UnitSpeed;
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AEnemy::Death() {
	ClearAttached();
	Destroy();
}

float AEnemy::GetUnitDamage() {
	return UnitDamage;
}

void AEnemy::EnterBaseCore() {
	OnBaseEntered.Broadcast(this);
	ClearAttached();
	Destroy();
}

void AEnemy::MoveToWaypoint() {
	// Move 
	AEnemyAIController* EnemyAIController = Cast<AEnemyAIController>(GetController());
	// self note, dont forget to change in indiv BP from AICont to EnemyAICont
	if (EnemyAIController) {
		if (CurrentWaypoint <= PathPoints.Num()) {
			for (AActor* Waypoint : PathPoints) {
				AWaypoint* waypoint = Cast<AWaypoint>(Waypoint);

				if (waypoint) {
					if (waypoint->GetWaypointOrder() == CurrentWaypoint) {
						EnemyAIController->MoveToActor(waypoint,  2.5f,  false );
						CurrentWaypoint++;
						break;
					}
				}
			}
		}
	}
}

void AEnemy::ClearAttached() {
	TArray<AActor*> AttachedActors;

	GetAttachedActors(AttachedActors);

	for (int x = 0; x < AttachedActors.Num(); x++)
	{
		AttachedActors[x]->Destroy();
	}
}
