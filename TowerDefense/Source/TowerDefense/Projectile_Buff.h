// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Projectile.h"
#include "Projectile_Buff.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API AProjectile_Buff : public AProjectile
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ABuff> BuffToSpawn;

	UFUNCTION(BlueprintCallable)
		void ApplyBuff(AActor* Target);

	bool hasBuff(AActor* Target);
};
