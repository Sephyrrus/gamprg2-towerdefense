// Fill out your copyright notice in the Description page of Project Settings.


#include "WaveData.h"
#include "Enemy.h"	

int32 UWaveData::TotalUnitCount() {
	for(int x = 0; x < Enemies.Num() - 1; x++) {
		numSpawns = Enemies[x].EnemyCount;
	}

	return numSpawns;
}

int32 UWaveData::TotalKillReward() {
	return TotalUnitCount() * KillReward;
}