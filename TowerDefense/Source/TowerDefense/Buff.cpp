// Fill out your copyright notice in the Description page of Project Settings.


#include "Buff.h"
#include "Enemy.h"
#include "Tower.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "TimerManager.h"

// Sets default values
ABuff::ABuff()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABuff::BeginPlay()
{
	Super::BeginPlay();
	CurrentDuration = BaseDuration;
	if (!hasAffected) {
		TriggerBuff();
	}
		
}

// Called every frame
void ABuff::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABuff::TriggerBuff() {

	BuffEffect();

	if (hasDuration && CurrentDuration > 0) {
		FTimerHandle TimerHandler;
		FTimerDelegate TimerDel;

		TimerDel.BindLambda([this] {
			TriggerBuff();
			CurrentDuration--;
		});

		GetWorldTimerManager().SetTimer(TimerHandler, TimerDel, 1.0f, false);
	}
	else if (hasDuration && CurrentDuration == 0) {
		Destroy();
	}
}

void ABuff::RefreshBuffDuration() {
	CurrentDuration = BaseDuration;
}