// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Enemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBaseEntered, AEnemy*, Enemy);

UENUM()
enum EnemyType
{
	Ground UMETA(DisplayName = "Ground"),
	Flying  UMETA(DisplayName = "Flying"),
};

UCLASS()
class TOWERDEFENSE_API AEnemy : public APawn
{
	GENERATED_BODY()
public:
	// Sets default values for this pawn's properties
	AEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UFloatingPawnMovement* Movement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* HitBox;
		
	UFUNCTION()
		void Death();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unit Stats")
		float UnitDamage = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unit Stats")
		float UnitSpeed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unit Stats")
		bool isBoss;

	void ClearAttached();
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<class AWaypoint*> PathPoints;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		class UHealth* Health;
	UPROPERTY(BlueprintAssignable, Category = "Event dispatchers")
		FOnBaseEntered OnBaseEntered;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unit Stats")
		TEnumAsByte<EnemyType> enemyType;

	void MoveToWaypoint();
	void EnterBaseCore();
	float GetUnitDamage();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int CurrentWaypoint;
};
