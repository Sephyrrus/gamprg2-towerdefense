// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WaveData.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FEnemySpawnConfig {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class AEnemy> Enemy;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 EnemyCount;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 EnemySpawnPoint;
};

UCLASS(BlueprintType)
class TOWERDEFENSE_API UWaveData : public UDataAsset
{
	GENERATED_BODY()
protected:
	int32 numSpawns = 0;
public: 
	UPROPERTY(EditAnywhere, BlueprintReadOnly) 
	float WaveDuration;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FEnemySpawnConfig> Enemies;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 KillReward;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 WaveClearReward;

	UFUNCTION(BlueprintPure)
	int32 TotalUnitCount();

	UFUNCTION(BlueprintPure)
	int32 TotalKillReward();
};
