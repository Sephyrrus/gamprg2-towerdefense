// Fill out your copyright notice in the Description page of Project Settings.


#include "Salvager.h"
#include "Enemy.h"
#include "Health.h"
#include "TDPlayer.h"
#include "Engine/World.h"
#include "Engine/Engine.h"

void ASalvager::BeginPlay()
{
	Super::BeginPlay();
	Player = Cast<ATDPlayer>(GetWorld()->GetFirstPlayerController()->GetPawn());
}

void ASalvager::AcquireTarget(AActor* Target) {
	AEnemy* Enemy = Cast<AEnemy>(Target);

	if (Enemy && isActive) {
		EnemiesInRange.Add(Enemy);
		TowerAction();
	}
}

void ASalvager::LoseTarget(AActor* Target) {
	AEnemy* Enemy = Cast<AEnemy>(Target);

	if (Enemy) {
		EnemiesInRange.Remove(Enemy);
	}
}

void ASalvager::TowerAction() {
	if (!isActive) return;

	for (int x = 0; x < EnemiesInRange.Num(); x++) {
		EnemiesInRange[x]->Health->OnDeath.AddDynamic(this, &ASalvager::GenerateGold);
	}
}

void ASalvager::GenerateGold() {
	Player->AddGold(TowerPower);
}

void ASalvager::OnBuild() {
	Player->OnSalvagerBuilt.Broadcast(TowerPower);
}

void ASalvager::OnTowerDestroy() {
	Player->OnSalvagerDestroyed.Broadcast(TowerPower);
}