// Fill out your copyright notice in the Description page of Project Settings.


#include "TowerBuildNode.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"
#include "BuildManager.h"
// Sets default values
ATowerBuildNode::ATowerBuildNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(SceneComponent);

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMesh->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void ATowerBuildNode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATowerBuildNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

