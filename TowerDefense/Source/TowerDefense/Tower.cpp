// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Materials/Material.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "TowerDataAsset.h"
#include "Components/ArrowComponent.h"
#include "Materials/Material.h"

// Sets default values
ATower::ATower()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene Component"));
	SetRootComponent(SceneComponent);

	TowerBaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret Base Mesh"));
	TowerBaseMesh->SetupAttachment(SceneComponent);

	TowerGunMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Turret Gun Mesh"));
	TowerGunMesh->SetupAttachment(SceneComponent);

	FirePoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Turret Fire Point"));
	FirePoint->SetupAttachment(TowerGunMesh);

	TowerRange = CreateDefaultSubobject<USphereComponent>(TEXT("Turret Range"));
	TowerRange->SetupAttachment(SceneComponent);

	TowerRange->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnOverlapBegin);
	TowerRange->OnComponentEndOverlap.AddDynamic(this, &ATower::OnOverlapExit);
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	InitializeTower();
	GetOverlapping();
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATower::InitializeTower() {
	if (TowerData) {
		TowerName = TowerData->TowerName;
		TowerRange->SetSphereRadius(TowerData->TowerRange[0]);
		TowerFireRate = TowerData->TowerFireRate[0];
		TowerPower = TowerData->TowerPower[0];
		UpgradeCost = TowerData->UpgradeCost[0];
		TowerCost = TowerData->TowerCost;
		MaxUpgrades = TowerData->MaxUpgrades;
	}
}

void ATower::GetOverlapping() {
	TArray<AActor*> OverlappedActors;

	TowerRange->GetOverlappingActors(OverlappedActors);

	for (int x = 0; x < OverlappedActors.Num(); x++) {
		AcquireTarget(OverlappedActors[x]);
	}
}

void ATower::ChangeMaterial (class UMaterial* Material) {
	TowerGunMesh->SetMaterial(0, Material);
	TowerBaseMesh->SetMaterial(0, Material);
}

void ATower::TowerAction()
{
}

void ATower::UpgradeTower() {
	if (CurrentUpgrades  >=  MaxUpgrades) return;
	TowerCost += UpgradeCost;
	CurrentUpgrades++;

	TowerRange->SetSphereRadius(TowerData->TowerRange[CurrentUpgrades]);
	TowerFireRate = TowerData->TowerFireRate[CurrentUpgrades];
	TowerPower = TowerData->TowerPower[CurrentUpgrades];
	UpgradeCost = TowerData->UpgradeCost[CurrentUpgrades];

	TowerBaseMesh->SetMaterial(0, UpgradedMat);
	TowerGunMesh->SetMaterial(0, UpgradedMat);

	// Refreshes Target List with new Range
	GetOverlapping();
}

int ATower::GetSellPrice() {
	return TowerCost / 2;
}

void ATower::LookAtTarget(FVector TargetLocation) {
	FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(TowerGunMesh->GetComponentLocation(), TargetLocation);
	TowerGunMesh->SetRelativeRotation(FRotator(0 , NewRotation.Yaw - 90, NewRotation.Pitch + 15));
}

void ATower::AcquireTarget(AActor* Target) {
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Base AcquireTarget"));
}

void ATower::LoseTarget(AActor* Target) {
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Base LoseTarget"));
}

void ATower::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	if (isActive) {
		AcquireTarget(OtherActor);
	}
	
}

void ATower::OnOverlapExit(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	if (isActive) {
		LoseTarget(OtherActor);
	}
}

